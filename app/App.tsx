import { Provider } from 'mobx-react';
import React from 'react';
import { Main } from './pages';
import AppState from './stores/AppState';
import TrackState from './stores/TrackState';

const rootStores = {
  appState: AppState,
  trackState: TrackState,
};

export default class App extends React.Component<any, any> {

  public render() {
    return (
      <Provider {...rootStores}>
        <Main/>
      </Provider>
    );
  }
}
