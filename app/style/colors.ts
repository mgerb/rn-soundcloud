export const colors = {
  fontPrimary: '#999999',
  fontLight: '#f2f2f2',

  highlight: '#f9f9f9',

  dark: '#3a3f41',
  green: '#6ae368',
  orange: '#f50',
  white: '#fff',
};
