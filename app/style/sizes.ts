export const sizes = {
  fontTiny: 10,
  fontSmall: 12,
  fontMedium: 15,
  fontLarge: 20,
};
