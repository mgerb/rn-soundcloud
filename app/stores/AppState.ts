import { action, computed, observable, runInAction } from 'mobx';
import UserAPI from '../api/user';
import { UserModel } from '../model';
import storage from '../utils/storage';

export class AppState {
  @observable public accessToken: string;
  @observable public user: UserModel;

  @computed
  public get authenticated(): boolean {
    return !!this.accessToken;
  }

  @action
  public async checkStorage() {
    const token = await storage.getAccessToken();
    if (token) {
      runInAction(() => {
        this.accessToken = token;
        this.login(token);
      });
    }
  }

  @action
  public async login(token: string) {
    this.accessToken = token;
    await storage.storeAccessToken(token);

    try {
      const me = await UserAPI.me();
      runInAction(() => {
        this.user = me;
      });
    } catch {
      console.log('error');
    }
  }

  @action
  public logout() {
    this.accessToken = undefined;
    this.user = undefined;
    storage.removeAccessToken();
  }

}

export default new AppState();
