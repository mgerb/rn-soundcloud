import * as _ from 'lodash';
import { action, computed, observable, runInAction } from 'mobx';
import TrackAPI from '../api/track';
// import UserAPI from '../api/user';
import { TrackEntityModel, TrackModel } from '../model';

export class TrackState {
  @observable public loading: boolean;
  @observable public playingTrack: TrackModel;

  @observable private _trackList: TrackModel[] = [];
  @observable private _trackQueue: TrackModel[] = [];

  // keep track of next_href for paging
  private next_href: string;

  // GETTERS for arrays
  @computed get trackList(): TrackModel[] {
    return this._trackList.slice();
  }

  @computed get trackQueue(): TrackModel[] {
    return this._trackQueue.slice();
  }

  // ACTIONS
  @action
  public async getTracks() {
    this.loading = true;
    const trackEntity = await TrackAPI.getTracks();
    runInAction(() => {
      this._trackList = this.filterTracks(trackEntity);
      this.loading = false;
    });
  }

  @action
  public async getNextTracks() {
    this.loading = true;
    const trackEntity = await TrackAPI.getTracksByFullUrl(this.next_href);
    runInAction(() => {
      // append tracks to current track list
      this._trackList = [...this.trackList, ...this.filterTracks(trackEntity)];
      this.loading = false;
    });
  }

  // perform track filtering based end point
  private filterTracks(entity: TrackEntityModel): TrackModel[] {
    // set next url for paging
    this.next_href = entity.next_href;
    return entity.collection;
  }

  @action
  public toggleFavorite(track: TrackModel) {

    // set the state of the track in current list
    if (track.user_favorite) {
      TrackAPI.unfavoriteTrack(track.id);
    } else {
      TrackAPI.favoriteTrack(track.id);
    }

    this._trackList = _.map(this.trackList, (t: TrackModel) => {
      if (t.id === track.id) {
        t.user_favorite = !t.user_favorite;
      }
      return t;
    });

  }

}

export default new TrackState();
