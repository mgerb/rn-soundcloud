import * as _ from 'lodash';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { Modal, ScrollView, StyleSheet, Text, TextStyle, TouchableHighlight, View, ViewStyle } from 'react-native';
import { Authenticate } from '../pages/Authenticate';
import { AppState } from '../stores/AppState';
import { colors, sizes } from '../style';

interface Props {
  appState?: AppState;
}

interface State {
  showModal: boolean;
}

const navItems = [{
  filters: [{
    display: '7 Days',
    query: {
      'created_at[from]': 'test',
    },
  }],
  title: 'Stream',
}, {
  title: 'House',
}, {
  title: 'Chill',
}, {
  title: 'Playlists',
}];

@inject('appState')
@observer
export class LeftNav extends React.Component<Props, State> {

  constructor() {
    super();
    this.state = {
      showModal: false,
    };
  }

  private renderHeader() {
    const username = _.get(this.props, 'appState.user.username');
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>{username}</Text>
      </View>
    );
  }

  private renderNavItems() {
    return navItems.map((item: any, index: number) => {
      return (
        <TouchableHighlight
          style={styles.navItem}
          key={index}
          onPress={() => console.log('test')}
          activeOpacity={1}
          underlayColor={colors.orange}
        >
          <View>
            <Text style={styles.navItemText}>{item.title}</Text>
          </View>
        </TouchableHighlight>
      );
    });
  }

  public render() {
    return(
      <View style={styles.container}>
        <Modal animationType='slide' visible={this.state.showModal}>
          <Authenticate onClose={() => this.setState({showModal: false})}/>
        </Modal>
        {this.renderHeader()}
        <ScrollView style={{flex: 1}}>
          {this.renderNavItems()}
        </ScrollView>
        <Text onPress={() => this.setState({showModal: true})}>Login</Text>
        <Text onPress={() => this.props.appState.logout()}>Login</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: colors.dark,
    flex: 1,
  } as ViewStyle,

  header: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  } as ViewStyle,

  headerText: {
    color: colors.white,
    fontSize: sizes.fontLarge,
  } as TextStyle,

  navItem: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,

  navItemText: {
    color: colors.white,
    fontSize: sizes.fontMedium,
  } as TextStyle,

});
