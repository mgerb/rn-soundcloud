import React from 'react';
import { ScrollView, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';
import { colors } from '../style';

export class FilterBar extends React.Component<any, any> {
  public render() {
    return(
      <View style={styles.container}>
        <ScrollView style={{flex: 1}} horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={[styles.item, styles.itemSelected]}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.text}>Test 123</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 30,
    backgroundColor: colors.white,
  } as ViewStyle,
  item: {
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
  } as ViewStyle,
  itemSelected: {
    borderBottomWidth: 1,
    borderColor: colors.orange,
  } as ViewStyle,
  text: {
    color: colors.fontPrimary,
  } as TextStyle,
});
