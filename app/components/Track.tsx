import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextStyle,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TrackModel } from '../model';
import { colors, sizes } from '../style';
import Player from '../utils/player';
import { Swiper } from './Swiper';

import { inject, observer } from 'mobx-react';
import { AppState } from '../stores/AppState';
import { TrackState } from '../stores/TrackState';

interface Props {
  track: TrackModel;
  appState?: AppState;
  trackState?: TrackState;
}

interface State {
  pressActive: boolean;
}

@inject('appState', 'trackState')
@observer
export class Track extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      pressActive: false,
    };
  }

  private imagePlaceholder() {
    return (
      <View style={[styles.imagePlaceholder, styles.imageShadow]}>
        <Icon name='play' color={colors.fontPrimary} size={sizes.fontLarge} />
      </View>
    );
  }

  private insertImage(url?: string) {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.playSound()}
        onPressIn={() => this.setState({ pressActive: true })}
        onPressOut={() => this.setState({ pressActive: false })}
      >
        <View style={{ paddingRight: 10 }}>
          {url ? (
            <View style={styles.imageShadow}>
              <Image
                style={styles.image}
                resizeMode={'cover'}
                source={{ uri: url }}
              />
            </View>
          ) : (
            this.imagePlaceholder()
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  private insertInfoItem(item: string, icon: string, userFavorite?: boolean) {
    const color = userFavorite ? colors.orange : colors.fontPrimary;
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon name={icon} color={color} size={sizes.fontTiny} />
        <Text style={[styles.infoText, { paddingLeft: 5 }]} numberOfLines={1}>
          {item}
        </Text>
      </View>
    );
  }

  private getTrackDuration(duration: number): string {
    // duration is in milliseconds
    duration = duration / 1000;
    const seconds = ('0' + Math.floor(duration % 60)).slice(-2);
    const minutes = Math.floor(duration / 60);
    return `${minutes}:${seconds}`;
  }

  private formatNumber(num: number): string {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  private playSound(): void {
    Player.playSound(this.props.track);
  }

  private favoriteTrack() {
    this.props.trackState.toggleFavorite(this.props.track);
  }

  public render() {
    const { track } = this.props;
    const { pressActive } = this.state;

    return (
      <Swiper
        leftThreshold={70}
        rightThreshold={70}
        onRightEvent={() => this.favoriteTrack()}
        style={[
          styles.container,
          {backgroundColor: pressActive ? colors.highlight : '#fff'},
        ]}
      >
        {this.insertImage(track.artwork_url)}

        <View style={{ flex: 1 }}>
          {/* title and artist */}
          <TouchableWithoutFeedback
            onPress={() => this.playSound()}
            onPressIn={() => this.setState({ pressActive: true })}
            onPressOut={() => this.setState({ pressActive: false })}
          >
            <View style={{ flex: 1 }}>
              <Text style={styles.trackTitle} numberOfLines={1}>
                {track.title}
              </Text>
              <Text style={styles.infoText} numberOfLines={1}>
                {track.user.username}
              </Text>
            </View>
          </TouchableWithoutFeedback>

          {/* play count and track time */}
          <View style={{flexDirection: 'row'}}>
            {this.insertInfoItem(this.formatNumber(track.playback_count), 'play')}
            <View style={{marginLeft: 10}}>
              {this.insertInfoItem(this.formatNumber(track.likes_count), 'heart', track.user_favorite)}
            </View>
            <View style={{marginLeft: 10}}>
              {this.insertInfoItem(this.getTrackDuration(track.duration), 'clock-o')}
            </View>
          </View>
        </View>
        <View style={{paddingHorizontal: 10}}>
          <Icon name='ellipsis-v' color={colors.fontPrimary} size={sizes.fontLarge} />
        </View>
      </Swiper>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    minHeight: 60,
    padding: 10,
    borderTopWidth: 1,
    borderTopColor: colors.highlight,
  } as ViewStyle,

  imageShadow: {
    borderRadius: 4,
    shadowColor: '#000000',
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowRadius: 2,
    shadowOpacity: 0.5,
  } as ViewStyle,

  image: {
    height: 60,
    width: 60,
    borderRadius: 4,
  } as ViewStyle,

  imagePlaceholder: {
    height: 60,
    width: 60,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.highlight,
  } as ViewStyle,

  infoText: {
    color: colors.fontPrimary,
    fontSize: sizes.fontTiny,
  } as TextStyle,

  trackTitle: {
    fontSize: sizes.fontSmall,
    color: colors.fontPrimary,
  } as TextStyle,
});
