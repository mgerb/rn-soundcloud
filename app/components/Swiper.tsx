import * as _ from 'lodash';
import React from 'react';
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  ViewStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../style/colors';
import { sizes } from '../style/sizes';

const { width } = Dimensions.get('window');

interface Props {
  leftThreshold: number;
  rightThreshold: number;
  onRightEvent?();
  onLeftEvent?();
  style: any;
}

interface State {
  leftActivated: boolean;
  rightActivated: boolean;
}

export class Swiper extends React.Component<Props, State> {

  constructor() {
    super();
    this.state = {
      leftActivated: false,
      rightActivated: false,
    };
  }

  private renderLeftButton(): any {
    const color = this.state.leftActivated ? colors.green : colors.fontPrimary;
    return (
      <View style={[styles.swipeItem, {backgroundColor: color}]}>
        <Text style={{color: colors.fontLight}}>123</Text>
      </View>
    );
  }

  private renderRightButton(): any {
    const color = this.state.rightActivated ? colors.orange : colors.fontPrimary;
    return (
      <View style={[styles.swipeItem, {alignItems: 'flex-end', backgroundColor: color}]}>
        <Icon name={'heart'} color={colors.fontLight} size={sizes.fontLarge} />
      </View>
    );
  }

  // check for offset and perform action
  private scrollRelease(): void {
    if (this.props.onLeftEvent && this.state.leftActivated) {
      this.props.onLeftEvent();
    } else if (this.props.onRightEvent && this.state.rightActivated) {
      this.props.onRightEvent();
    }
  }

  private onScroll(e: any): void {
    const { x } = e.nativeEvent.contentOffset;

    if (!x) { return; }

    let newState: any = {
      leftActivated: x < -this.props.leftThreshold,
      rightActivated: x > this.props.rightThreshold,
    };

    if (!_.isEqual(newState, this.state)) {
      this.setState(newState);
    }

  }

  public render() {
    return (
      <View>
        <ScrollView
          automaticallyAdjustContentInsets={true}
          directionalLockEnabled={true}
          horizontal={true}
          onMomentumScrollBegin={() => this.scrollRelease()}
          onScroll={e => this.onScroll(e)}
          scrollEventThrottle={5}
        >
          {/* render the children here */}
          <View style={[this.props.style, styles.container]}>
            {this.props.children}
          </View>
        </ScrollView>

        {/* background buttons */}
        <View style={[styles.background]}>
          {this.renderLeftButton()}
          {this.renderRightButton()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width,
  } as ViewStyle,
  background: {
    alignItems: 'center',
    flexDirection: 'row',
    height: '100%',
    position: 'absolute',
    top: 0,
    width,
    zIndex: -1,
  } as ViewStyle,
  swipeItem: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    padding: 20,
  } as ViewStyle,
});
