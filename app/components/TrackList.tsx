import { inject, observer } from 'mobx-react';
import React from 'react';
import { ListView, StyleSheet, View, ViewStyle } from 'react-native';
import { TrackState } from '../stores/TrackState';
import { colors } from '../style';
import { Track } from './Track';

interface Props {
  trackState?: TrackState;
}

interface State {}

@inject('trackState')
@observer
export class TrackList extends React.Component<Props, State> {

  private dataSource = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2,
  });

  private endReached() {
    // this may get called at beginning of page load
    // prevent from calling next tracks
    if (!this.props.trackState.loading) {
      this.props.trackState.getNextTracks();
    }
  }

  public render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.dataSource.cloneWithRows(this.props.trackState.trackList)}
          enableEmptySections={true}
          onEndReached={() => this.endReached()}
          onEndReachedThreshold={1}
          pageSize={10}
          renderRow={rowData => <Track track={rowData}/>}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  } as ViewStyle,
});
