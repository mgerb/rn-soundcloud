import { AsyncStorage } from 'react-native';
// import { UserModel } from '../model';

// const userKey = 'userKey';
const accessTokenKey = 'accessTokenKey';

class Storage {

  // public async storeUser(user: UserModel) {
  //   return await AsyncStorage.setItem(userKey, JSON.stringify(user));
  // }

  // public async getUser(): Promise<UserModel> {
  //   const user = await AsyncStorage.getItem(userKey);
  //   return JSON.parse(user);
  // }

  public async removeAccessToken() {
    return await AsyncStorage.removeItem(accessTokenKey);
  }

  public async storeAccessToken(token: string) {
    return await AsyncStorage.setItem(accessTokenKey, token);
  }

  public async getAccessToken(): Promise<string> {
    return await AsyncStorage.getItem(accessTokenKey);
  }
}

export default new Storage();
