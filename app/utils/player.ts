import Sound from 'react-native-sound';
import TrackAPI from '../api/track';
import { TrackModel } from '../model';

class Player {

  public sound: Sound;

  constructor() {
    Sound.setCategory('Playback', false);
    Sound.setActive(true);
  }

  public async playSound(track: TrackModel) {
    const streamUrl = await TrackAPI.getSoundUrl(track.uri);

    if (this.sound) {
      await this.sound.stop();
    }

    this.sound = new Sound(streamUrl, '', (error: any) => {
      if (!error) {
        this.sound.play(() => {
          // Release when it's done so we're not using up resources
          this.sound.release();
        });
      }
    });
  }

}

export default new Player();
