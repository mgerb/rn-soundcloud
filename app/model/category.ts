export interface CategoryModel {
  filters: FilterModel[];
  selectedFilter: FilterModel;
  type: string;
}

export interface FilterModel {
  displayText: string;
  query: Map<string, string>;
}
