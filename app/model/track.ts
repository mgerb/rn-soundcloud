import { PublisherMetadataModel } from './publisher_metadata';
import { UserModel } from './user';

export interface TrackEntityModel {
  collection: TrackModel[];
  future_href?: string;
  next_href: string;
}

export interface TrackModel {
  artwork_url: string;
  comment_count: number;
  commentable: boolean;
  created_at: string;
  description: string;
  display_date: string; // 2017-08-25T00:00:00Z
  download_count: number;
  download_url?: string;
  downloadable: boolean;
  duration: number;
  embeddable_by: string;
  full_duration: number;
  genre: string;
  has_downloads_left: boolean;
  id: number;
  kind: string;
  label_name: string;
  last_modified: string;
  license: string;
  likes_count: number;
  monetization_model: string;
  permalink: string;
  permalink_url: string;
  playback_count: number;
  policy: string;
  public: boolean;
  publisher_metadata: PublisherMetadataModel;
  purchase_title?: string;
  purchase_url?: string;
  release_date: string;
  reposts_count: number;
  secret_token?: any;
  sharing: string;
  state: string;
  streamable: boolean;
  tag_list: string;
  title: string;
  uri: string;
  urn: string;
  user: UserModel;
  user_favorite?: boolean;
  user_id: number;
  user_playback_count?: number;
  visuals?: any;
  waveform_url: string;
}
