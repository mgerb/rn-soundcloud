import { QuotaModel } from './quota';

export interface UserModel {
  avatar_url: string;
  city: string;
  country?: string;
  country_code?: any;
  description?: string,
  discogs_name?: string;
  first_name: string;
  followers_count?: number;
  followings_count?: number;
  full_name: string;
  id: number;
  kind: string;
  last_modified: string;
  last_name: string;
  locale?: string;
  myspace_name?: string;
  online?: boolean;
  permalink: string;
  permalink_url: string;
  plan?: string;
  playlist_count?: number;
  primary_email_confirmed?: boolean;
  private_playlists_count?: number;
  private_tracks_count?: number;
  quota?: QuotaModel;
  reposts_count?: number;
  subscriptions?: any[];
  track_count?: number;
  upload_seconds_left?: number;
  uri: string;
  urn?: string;
  username: string;
  verified?: boolean;
  website?: string;
  website_title?: string;
}
