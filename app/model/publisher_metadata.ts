export interface PublisherMetadataModel {
  album_title: string;
  artist: string;
  c_line: string;
  c_line_for_display: string;
  contains_music: boolean;
  explicit: boolean;
  id: number;
  isrc: string;
  p_line: string;
  p_line_for_display: string;
  release_title: string;
  upc_or_ean: string;
  urn: string;
}
