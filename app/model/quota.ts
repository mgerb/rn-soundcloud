export interface QuotaModel {
  unlimited_upload_quota: boolean;
  upload_seconds_left: number;
  upload_seconds_used: number;
}
