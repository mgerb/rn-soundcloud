import moment from 'moment';
import URI from 'urijs';
import { TrackEntityModel } from '../model';
import { api } from './api';

class TrackAPI {

  public async getTracksByFullUrl(url: string): Promise<TrackEntityModel> {
    const uri = URI.parse(url);
    const path = uri.path + '?' + uri.query;
    try {
      const response = await api.get(path);
      return response.data;
    } catch (err) {
      return null;
    }
  }

  public async getTracks(): Promise<TrackEntityModel> {
    const date = moment().subtract(7, 'd').format('YYYY-MM-DD hh:mm:ss');

    try {
      const response = await api.get(`/tracks?`
        + `&linked_partitioning=1`
        + `&tags=dubstep`
        + `&region=soundcloud:regions:US`
        + `&offset=0`
        + `&limit=50`
        + `&created_at[from]=${date}`,
      );

      return response.data;

    } catch (err) {
      return null;
    }

  }

  public async favoriteTrack(trackID: number): Promise<any> {
    try {
      return await api.put(`/me/favorites/${trackID}`);
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  public async unfavoriteTrack(trackID: number): Promise<any> {
    try {
      return await api.delete(`/me/favorites/${trackID}`);
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  public async getSoundUrl(url: string): Promise<string> {
    try {
      const response = await api.get(`${url}/streams`);
      return response.data.http_mp3_128_url;
    } catch (err) {
      return null;
    }
  }
}

export default new TrackAPI();
