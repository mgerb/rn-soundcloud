import axios from 'axios';
import URI from 'urijs';
import AppState from '../stores/AppState';

export const ClientID: string = 'e582b63d83a5fb2997d1dbf2f62705da';
const baseUrl: string = 'https://api.soundcloud.com';

export const api = axios.create({
  baseURL: baseUrl,
  headers: {},
  timeout: 1000,
});

// request middleware
api.interceptors.request.use(config => {
  // add client id OR auth token to request
  let params: any = {};
  if (!!AppState.accessToken) {
    params.oauth_token = AppState.accessToken;
  } else {
    params.client_id = ClientID;
  }

  config.url = URI(config.url).addQuery(params).toString();

  return config;
});

// response middleware
api.interceptors.response.use(config => {
  return config;
});
