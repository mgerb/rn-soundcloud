import { UserModel } from '../model';
import { api } from './api';

class UserAPI {

  public async me(): Promise<UserModel> {
    try {
      const response = await api.get('/me');
      return response.data as UserModel;
    } catch (err) {
      return null;
    }
  }

  public async activities(): Promise<any> {
    try {
      const response = await api.get('/me/activities?limit=100');
      return response.data;
    } catch (err) {
      return null;
    }
  }
}

export default new UserAPI();
