import { inject, observer } from 'mobx-react';
import React from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle, WebView } from 'react-native';
import URI from 'urijs';
import { ClientID } from '../api/api';
import { colors, sizes } from '../style';

import { AppState } from '../stores/AppState';

interface Props {
  appState?: AppState;
  onClose();
}

@inject('appState')
@observer
export class Authenticate extends React.Component<Props, any> {

  private url: string = 'https://soundcloud.com/connect?'
    + `client_id=${ClientID}`
    + '&redirect_uri=https%3A%2F%2Fsoundredux.io%2Fapi%2Fcallback'
    + '&response_type=code_and_token'
    + '&scope=non-expiring'
    + '&state=SoundCloud_Dialog_1b45';

  public onLoad(e: any) {

    if (!!e.nativeEvent.url.match(/#access_token=/)) {
      const query = e.nativeEvent.url.split('#')[1];
      const params: any = URI.parseQuery(query);
      this.props.appState.login(params.access_token);
      this.props.onClose();
    }
  }

  private renderHeader() {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText} onPress={() => this.props.onClose()}>Close</Text>
      </View>
    );
  }

  public render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        <WebView source={{uri: this.url}} onLoadStart={(e) => this.onLoad(e)}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  } as ViewStyle,
  header: {
    height: 50,
    paddingHorizontal: 10,
    justifyContent: 'center',
  } as ViewStyle,
  headerText: {
    color: colors.orange,
    fontSize: sizes.fontLarge,
  } as TextStyle,
});
