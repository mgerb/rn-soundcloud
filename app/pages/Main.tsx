import { inject, observer } from 'mobx-react';
import React from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { FilterBar, LeftNav, TrackList } from '../components';
import { AppState } from '../stores/AppState';
import { TrackState } from '../stores/TrackState';
import { colors, sizes } from '../style';

interface Props {
  appState?: AppState;
  trackState?: TrackState;
}

interface State {
  open: boolean;
  ready: boolean;
}

@inject('appState', 'trackState')
@observer
export class Main extends React.Component<Props, State> {

  constructor() {
    super();
    this.state = {
      open: false,
      ready: false,
    };
  }

  public componentDidMount() {
    this.bootstrap();
  }

  private async bootstrap() {
    // check local storage for access token before bootstrapping
    await this.props.appState.checkStorage();
    this.setState({ready: true});
    this.afterBootstrap();
  }

  private afterBootstrap() {
    this.props.trackState.getTracks();
  }

  private renderHeader() {
    return (
      <View style={styles.header}>
        <Icon name='bars' style={styles.headerText} onPress={() => this.setState({open: !this.state.open})}/>
        <Text style={styles.headerText}>Title</Text>
        <Icon name='search' style={styles.headerText} onPress={() => console.log('search')}/>
      </View>
    );
  }

  public render() {
    const { open, ready } = this.state;

    // wait until bootstrap to load full app
    if (!ready) {
      return <View/>;
    }

    return (
      <View style={styles.container}>
        <Drawer
          content={<LeftNav/>}
          onClose={() => this.setState({open: false})}
          onOpen={() => this.setState({open: true})}
          open={open}
          openDrawerOffset={120}
          panOpenMask={15}
          tapToClose={true}
          type={'static'}
          tweenHandler={Drawer.tweenPresets.parallax}
        >
          <View style={{flex: 1}}>
            {this.renderHeader()}
            <FilterBar/>
            <TrackList/>
          </View>
        </Drawer>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingTop: 20,
  } as ViewStyle,

  header: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.dark,
    paddingHorizontal: 20,
  } as ViewStyle,

  headerText: {
    color: colors.fontLight,
    fontSize: sizes.fontLarge,
  } as TextStyle,

});
